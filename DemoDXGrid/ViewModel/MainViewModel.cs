﻿using DevExpress.Mvvm;
using DevExpress.Xpf.Core.ConditionalFormatting;
using DevExpress.Xpf.Editors.Settings;
using DevExpress.Xpf.Grid;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using System.Diagnostics;

namespace DemoDXGrid
{
  public class DataGridColumnDefinition
  {
    public string FieldName { get; set; }
    public string Header { get; set; }
    public string Width { get; set; }
    public string HeaderTemplate { get; set; }
    public FixedStyle Fixed { get; set; }
    public TextEditSettings EditSettings { get; set; }
  }

  public class DataGridBandDefinition
  {
    public string Header { get; set; }
    public ObservableCollection<DataGridColumnDefinition> ChildColumns { get; set; }
  }

  public class MainViewModel : ViewModelBase
  {

    private ObservableCollection<DataItem> _data = new ObservableCollection<DataItem>();
    private ObservableCollection<DataGridBandDefinition> _Columns = new ObservableCollection<DataGridBandDefinition>();
    private ObservableCollection<FormatCondition> _Conditions = new ObservableCollection<FormatCondition>();
    ICustomService SetPointCustomService => GetService<ICustomService>("SetPointCustomService");
    ICustomService setptcs = new CustomService();
    bool conditionalFormatToggle = false;

    public ICommand ToggleFormattingCommand { get; private set; }
    public ICommand ToggleFormat { get; private set; }

    private bool _formattingON = true;

    public string FormattingState
    {
      get
      {
        if (_formattingON)
        {
          return "Conditional Formatting ON";
        }
        else
        {
          return "Conditional Formatting OFF";
        }
      }
    }

    public string FormatState
    {
      get
      {
        if (conditionalFormatToggle)
        {
          return "Format A";
        }
        else
        {
          return "Format B";
        }
      }
    }

    public MainViewModel()
    {
      
      //SetPointCustomService.BeginUpdate(); 
      CreateColumns();
      LoadData();
      //SetPointCustomService.EndUpdate();
      ToggleFormattingCommand = new DelegateCommand(OnConditionalFormatting);
      ToggleFormat = new DelegateCommand(OnExternalFormatting);
    }

    void OnExternalFormatting() { OnFormatting(); }

    async void OnFormatting()
    {
      Stopwatch sw = new Stopwatch();

      sw.Start();
      SetPointCustomService.BeginUpdate();
      await ReloadSetpointGridAsync().ConfigureAwait(true);
      RaisePropertyChanged(() => FormatState);
      SetPointCustomService.EndUpdate();

      Console.WriteLine("Time taken to update = {0:0.0}ms", sw.ElapsedMilliseconds);
    }

    public void OnConditionalFormatting()
    {
      _formattingON = !_formattingON;
      SetPointCustomService.BeginConditionalFormattingUpdate();
      foreach (FormatCondition c in _Conditions)
      {
        c.IsEnabled = _formattingON;
      }

      RaisePropertyChanged(() => FormattingState);
      SetPointCustomService.EndConditionalFormattingUpdate();
    }

    /// <summary>
    ///   Return collection of RunGrid column objects.
    /// </summary>
    /// <returns></returns>
    async Task ReloadSetpointGridAsync()
    {
      await LoadSetpointGridAsync().ConfigureAwait(true);
    }

    Task<bool> LoadSetpointGridAsync()
    {
      Columns?.Clear();

      return Task.Run(
                async () =>
                {
                  try
                  {
                    if (await LoadSetpointGrid_DoWorkAsync().ConfigureAwait(true)) return true;
                  }
                  catch (Exception ex)
                  {
                    throw ex;
                  }

                  return false;
                });
    }

    async Task<bool> LoadSetpointGrid_DoWorkAsync()
    {
      ObservableCollection<DataItem> tempData = new ObservableCollection<DataItem>();
      for (int item = 0; item < 150; item++)
      {
        DataItem newItem = new DataItem();

        PropertyInfo[] pis = typeof(DataItem).GetProperties();

        double num = 0.003;
        foreach (PropertyInfo pi in pis)
        {
          num *= -1;
          newItem.GetType().GetProperty(pi.Name).SetValue(newItem, num, null);
        }

        tempData.Add(newItem);
      }

      await Application.Current.Dispatcher.BeginInvoke(new Action(() =>
      {
        Data.Clear();
        foreach (var o in tempData)
        {
          _data.Add(o);
        }
      }));

      //Reload data.
      await Application.Current.Dispatcher.BeginInvoke(
                                                       new Action(
                                                                  () =>
                                                                  {
                                                                    //Clear conditional formats - prevents it looking for data that isnt there.
                                                                    SetPointCustomService?.BeginConditionalFormattingUpdate();
                                                                    Conditions.Clear();
                                                                    SetPointCustomService?.EndConditionalFormattingUpdate();
                                                                    Columns = GetSetpointGridBands();
                                                                  }));

     await Application.Current.Dispatcher.BeginInvoke(new Action(() =>
      {
        ObservableCollection<FormatCondition> newConditions = GetConditionalGridFormats();
        SetPointCustomService?.BeginConditionalFormattingUpdate();
        _Conditions.Clear();
        foreach (var o in newConditions)
        {
          _Conditions.Add(o);
        }
        SetPointCustomService?.EndConditionalFormattingUpdate();
      }
      ));
      return true;
    }

    public ObservableCollection<FormatCondition> GetConditionalGridFormats()
    {
      ObservableCollection<FormatCondition> newConditions = new ObservableCollection<FormatCondition>();
      long i = 0;
      foreach (DataGridBandDefinition dgbd in Columns)
      {
        if (conditionalFormatToggle ^ i < 5)
        {
          foreach (DataGridColumnDefinition cc in dgbd.ChildColumns)
          {
            //Add conditional formatting for this column (disabled as standard)
            var low = new FormatCondition()
            {
              Expression = $"[RowData.Row.{cc.FieldName}] < -0.002",
              FieldName = $"RowData.Row.{cc.FieldName}",
              Format = new Format()
              {
                Foreground = Brushes.Blue
              },
              IsEnabled = _formattingON

            };

            var high = new FormatCondition()
            {
              Expression = $"[RowData.Row.{cc.FieldName}] > 0.002",
              FieldName = $"RowData.Row.{cc.FieldName}",
              Format = new Format()
              {
                Foreground = Brushes.Red
              },
              IsEnabled = _formattingON

            };
            newConditions.Add(low);
            newConditions.Add(high);

          }
        }
        i++;
      }
      conditionalFormatToggle = !conditionalFormatToggle;
      //Add the new conditions to the grid


      return newConditions;
    }

    public ObservableCollection<DataGridBandDefinition> GetSetpointGridBands()
    {
      ObservableCollection<DataGridBandDefinition> cols = new ObservableCollection<DataGridBandDefinition>();

      var decimal3Mask = new TextEditSettings();
      decimal3Mask.MaskUseAsDisplayFormat = true;
      decimal3Mask.MaskType = DevExpress.Xpf.Editors.MaskType.Numeric;
      decimal3Mask.HorizontalContentAlignment = EditSettingsHorizontalAlignment.Center;
      decimal3Mask.Mask = "F3";

      string[] letters = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K" };
      foreach (string letter in letters)
      {
        DataGridBandDefinition dgb = new DataGridBandDefinition()
        {
          Header = $"Header {letter}"
        };
        dgb.ChildColumns = new ObservableCollection<DemoDXGrid.DataGridColumnDefinition>();
        for (int numCols = 0; numCols < 5; numCols++)
        {

          dgb.ChildColumns.Add(new DataGridColumnDefinition() { FieldName = $"{letter}{numCols}", Header = numCols.ToString(), Width = "50", Fixed = FixedStyle.None, EditSettings = decimal3Mask });
        }


        cols.Add(dgb);
      }
      return cols;
    }

    public void CreateColumns()
    {
      Columns = GetSetpointGridBands();
      Conditions = GetConditionalGridFormats();

    }
    
    public void LoadData()
    {
      for (int item = 0; item < 150; item++)
      {
        DataItem newItem = new DataItem();

        PropertyInfo[] pis = typeof(DataItem).GetProperties();

        double num = 0.003;
        foreach (PropertyInfo pi in pis)
        {
          num *= -1;
          newItem.GetType().GetProperty(pi.Name).SetValue(newItem, num, null);
        }

        _data.Add(newItem);
      }
    }

    public ObservableCollection<DataItem> Data
    {
      get
      {
        return _data;
      }

      set
      {
        _data = value;
        RaisePropertyChanged();
      }
    }

    public ObservableCollection<DataGridBandDefinition> Columns
    {
      get
      {
        return _Columns;
      }

      set
      {
        _Columns = value;
        RaisePropertyChanged();
      }
    }

    public ObservableCollection<FormatCondition> Conditions
    {
      get
      {
        return _Conditions;
      }

      set
      {
        _Conditions = value;
        RaisePropertyChanged();
      }
    }
  }
}
