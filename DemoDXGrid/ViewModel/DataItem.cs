﻿using DevExpress.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoDXGrid
{
    public class DataItem : ViewModelBase
    {

        private double _A0 = 0;
        public double A0 { get { return _A0; } set { _A0 = value; RaisePropertyChanged(); } }
        private double _A1 = 0;
        public double A1 { get { return _A1; } set { _A1 = value; RaisePropertyChanged(); } }
        private double _A2 = 0;
        public double A2 { get { return _A2; } set { _A2 = value; RaisePropertyChanged(); } }
        private double _A3 = 0;
        public double A3 { get { return _A3; } set { _A3 = value; RaisePropertyChanged(); } }
        private double _A4 = 0;
        public double A4 { get { return _A4; } set { _A4 = value; RaisePropertyChanged(); } }


        private double _B0 = 0;
        public double B0 { get { return _B0; } set { _B0 = value; RaisePropertyChanged(); } }
        private double _B1 = 0;
        public double B1 { get { return _B1; } set { _B1 = value; RaisePropertyChanged(); } }
        private double _B2 = 0;
        public double B2 { get { return _B2; } set { _B2 = value; RaisePropertyChanged(); } }
        private double _B3 = 0;
        public double B3 { get { return _B3; } set { _B3 = value; RaisePropertyChanged(); } }
        private double _B4 = 0;
        public double B4 { get { return _B4; } set { _B4 = value; RaisePropertyChanged(); } }

        private double _C0 = 0;
        public double C0 { get { return _C0; } set { _C0 = value; RaisePropertyChanged(); } }
        private double _C1 = 0;
        public double C1 { get { return _C1; } set { _C1 = value; RaisePropertyChanged(); } }
        private double _C2 = 0;
        public double C2 { get { return _C2; } set { _C2 = value; RaisePropertyChanged(); } }
        private double _C3 = 0;
        public double C3 { get { return _C3; } set { _C3 = value; RaisePropertyChanged(); } }
        private double _C4 = 0;
        public double C4 { get { return _C4; } set { _C4 = value; RaisePropertyChanged(); } }

        private double _D0 = 0;
        public double D0 { get { return _D0; } set { _D0 = value; RaisePropertyChanged(); } }
        private double _D1 = 0;
        public double D1 { get { return _D1; } set { _D1 = value; RaisePropertyChanged(); } }
        private double _D2 = 0;
        public double D2 { get { return _D2; } set { _D2 = value; RaisePropertyChanged(); } }
        private double _D3 = 0;
        public double D3 { get { return _D3; } set { _D3 = value; RaisePropertyChanged(); } }
        private double _D4 = 0;
        public double D4 { get { return _D4; } set { _D4 = value; RaisePropertyChanged(); } }

        private double _E0 = 0;
        public double E0 { get { return _E0; } set { _E0 = value; RaisePropertyChanged(); } }
        private double _E1 = 0;
        public double E1 { get { return _E1; } set { _E1 = value; RaisePropertyChanged(); } }
        private double _E2 = 0;
        public double E2 { get { return _E2; } set { _E2 = value; RaisePropertyChanged(); } }
        private double _E3 = 0;
        public double E3 { get { return _E3; } set { _E3 = value; RaisePropertyChanged(); } }
        private double _E4 = 0;
        public double E4 { get { return _E4; } set { _E4 = value; RaisePropertyChanged(); } }


        private double _F0 = 0;
        public double F0 { get { return _F0; } set { _F0 = value; RaisePropertyChanged(); } }
        private double _F1 = 0;
        public double F1 { get { return _F1; } set { _F1 = value; RaisePropertyChanged(); } }
        private double _F2 = 0;
        public double F2 { get { return _F2; } set { _F2 = value; RaisePropertyChanged(); } }
        private double _F3 = 0;
        public double F3 { get { return _F3; } set { _F3 = value; RaisePropertyChanged(); } }
        private double _F4 = 0;
        public double F4 { get { return _F4; } set { _F4 = value; RaisePropertyChanged(); } }

        private double _G0 = 0;
        public double G0 { get { return _G0; } set { _G0 = value; RaisePropertyChanged(); } }
        private double _G1 = 0;
        public double G1 { get { return _G1; } set { _G1 = value; RaisePropertyChanged(); } }
        private double _G2 = 0;
        public double G2 { get { return _G2; } set { _G2 = value; RaisePropertyChanged(); } }
        private double _G3 = 0;
        public double G3 { get { return _G3; } set { _G3 = value; RaisePropertyChanged(); } }
        private double _G4 = 0;
        public double G4 { get { return _G4; } set { _G4 = value; RaisePropertyChanged(); } }


        private double _H0 = 0;
        public double H0 { get { return _H0; } set { _H0 = value; RaisePropertyChanged(); } }
        private double _H1 = 0;
        public double H1 { get { return _H1; } set { _H1 = value; RaisePropertyChanged(); } }
        private double _H2 = 0;
        public double H2 { get { return _H2; } set { _H2 = value; RaisePropertyChanged(); } }
        private double _H3 = 0;
        public double H3 { get { return _H3; } set { _H3 = value; RaisePropertyChanged(); } }
        private double _H4 = 0;
        public double H4 { get { return _H4; } set { _H4 = value; RaisePropertyChanged(); } }

        private double _I0 = 0;
        public double I0 { get { return _I0; } set { _I0 = value; RaisePropertyChanged(); } }
        private double _I1 = 0;
        public double I1 { get { return _I1; } set { _I1 = value; RaisePropertyChanged(); } }
        private double _I2 = 0;
        public double I2 { get { return _I2; } set { _I2 = value; RaisePropertyChanged(); } }
        private double _I3 = 0;
        public double I3 { get { return _I3; } set { _I3 = value; RaisePropertyChanged(); } }
        private double _I4 = 0;
        public double I4 { get { return _I4; } set { _I4 = value; RaisePropertyChanged(); } }

        private double _J0 = 0;
        public double J0 { get { return _J0; } set { _J0 = value; RaisePropertyChanged(); } }
        private double _J1 = 0;
        public double J1 { get { return _J1; } set { _J1 = value; RaisePropertyChanged(); } }
        private double _J2 = 0;
        public double J2 { get { return _J2; } set { _J2 = value; RaisePropertyChanged(); } }
        private double _J3 = 0;
        public double J3 { get { return _J3; } set { _J3 = value; RaisePropertyChanged(); } }
        private double _J4 = 0;
        public double J4 { get { return _J4; } set { _J4 = value; RaisePropertyChanged(); } }

        private double _K0 = 0;
        public double K0 { get { return _K0; } set { _K0 = value; RaisePropertyChanged(); } }
        private double _K1 = 0;
        public double K1 { get { return _K1; } set { _K1 = value; RaisePropertyChanged(); } }
        private double _K2 = 0;
        public double K2 { get { return _K2; } set { _K2 = value; RaisePropertyChanged(); } }
        private double _K3 = 0;
        public double K3 { get { return _K3; } set { _K3 = value; RaisePropertyChanged(); } }
        private double _K4 = 0;
        public double K4 { get { return _K4; } set { _K4 = value; RaisePropertyChanged(); } }

    }
}
