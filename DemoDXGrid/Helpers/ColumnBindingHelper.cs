﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace DemoDXGrid
{

        /// <summary>
        /// Class contains methods to bind FieldName descriptors from the dxgrid to Object properties.
        /// </summary>
        public class ColumnBindingHelper
        {
            public static string GetBindingPath(DependencyObject obj)
            {
                return (string)obj.GetValue(BindingPathProperty);
            }

            public static void SetBindingPath(DependencyObject obj, string value)
            {
                obj.SetValue(BindingPathProperty, value);
            }

            // Using a DependencyProperty as the backing store for BindingPath.  This enables animation, styling, binding, etc...
            public static readonly DependencyProperty BindingPathProperty =
                DependencyProperty.RegisterAttached("BindingPath", typeof(string), typeof(ColumnBindingHelper), new PropertyMetadata(null, OnBindingPathChanged));

            private static void OnBindingPathChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
            {
                var column = d as DevExpress.Xpf.Grid.GridColumn;
                if (column == null || e.NewValue == null)
                    return;
                column.Binding = new Binding(e.NewValue.ToString());
            }
        }
    
}
