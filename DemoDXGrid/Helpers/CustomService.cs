﻿using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Grid;
using System;
using System.Windows;


namespace DemoDXGrid
{
    public interface ICustomService
    {
        void BeginUpdate();
        void EndUpdate();
        void RefreshData();

        void BeginConditionalFormattingUpdate();
        void EndConditionalFormattingUpdate();
    }

    /// <summary>
    /// Class surfaces grid methods, enabling, for example, the methods to be called from a View Model.
    /// Enables MVVM access to BeginUpdate,EndUpdate for format conditons.
    /// </summary>
    public class CustomService : ServiceBase, ICustomService
    {
        public GridControl GridControl
        {
            get { return (GridControl)GetValue(GridControlProperty); }
            set { SetValue(GridControlProperty, value); }
        }

        public static readonly DependencyProperty GridControlProperty =
            DependencyProperty.Register("GridControl", typeof(GridControl), typeof(CustomService), new PropertyMetadata(null));

        /// <summary>
        /// Stops grid from responding to data changes
        /// </summary>
        /// <remarks>Runs synchronously on the ui thread</remarks>
        public void BeginUpdate()
        {
            Dispatcher.Invoke(() => GridControl?.BeginDataUpdate());

        }

        public void EndUpdate()
        {
            Dispatcher.Invoke(() => GridControl?.EndDataUpdate());
        }

        void ICustomService.BeginConditionalFormattingUpdate()
        {
            ((TableView)GridControl.View).FormatConditions.BeginUpdate();
        }
        void ICustomService.EndConditionalFormattingUpdate()
        {
            ((TableView)GridControl.View).FormatConditions.EndUpdate();
        }

        public void RefreshData()
        {
            Dispatcher.Invoke(() => GridControl?.RefreshData());
        }
    }
}